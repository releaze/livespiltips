<?php
/**
 * livespiltips functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package livespiltips
 */
if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'livespiltips_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function livespiltips_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on livespiltips, use a find and replace
		 * to change 'livespiltips' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'livespiltips', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'livespiltips' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'livespiltips_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'livespiltips_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function livespiltips_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'livespiltips_content_width', 640 );
}
add_action( 'after_setup_theme', 'livespiltips_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function livespiltips_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'livespiltips' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'livespiltips' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'livespiltips_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function livespiltips_scripts() {
	wp_enqueue_style( 'livespiltips-style', get_stylesheet_uri(), array(), _S_VERSION );

	wp_enqueue_script( 'livespiltips-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'livespiltips_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//======================================================================================================================
add_action( 'wp_enqueue_scripts', 'livespiltips_style_theme' );
add_action( 'after_setup_theme', 'livespiltips_main_menu' );
add_action( 'after_setup_theme', 'livespiltips_image_size' );
add_action( 'after_setup_theme', 'livespiltips_latest_large_image_size' );
add_action( 'after_setup_theme', 'livespiltips_latest_image_size' );
add_action( 'after_setup_theme', 'livespiltips_fullwidth_latest_image_size' );
add_action( 'widgets_init', 'livespiltips_register_sidebar' );
add_shortcode( 'latest_posts', 'livespiltips_latest_posts_shortcode_function' );
add_shortcode( 'featured_posts', 'livespiltips_featured_posts_shortcode_function' );
add_shortcode( 'fullwidth_latest_post', 'livespiltips_fullwidth_latest_post_shortcode_function' );

function livespiltips_style_theme(){
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}

function livespiltips_main_menu(){
    register_nav_menu( 'livespiltips_top_menu', 'Top Menu' );
}

function livespiltips_image_size(){
    add_image_size( 'post-thumb', 215, 135, true );
}

function livespiltips_latest_large_image_size(){
    add_image_size( 'latest-large-post-thumb', 498, 360, true );
}

function livespiltips_latest_image_size(){
    add_image_size( 'latest-post-thumb', 249, 180, true );
}

function livespiltips_recent_posts_image_size(){
    add_image_size( 'recent-posts-thumb', 91, 91, true );
}

function livespiltips_fullwidth_latest_image_size(){
    add_image_size( 'fullwidth-latest-post-thumb', 780, 407, true );
}

function livespiltips_register_sidebar(){
	register_sidebar( array(
		'name' => "Articles Sidebar",
		'id' => 'articles-sidebar',
		'description' => 'These widgets will be shown in the Articles Page.',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	) );

		register_sidebar( array(
        'name' => "Secondary Sidebar",
        'id' => 'secondary-sidebar',
        'description' => 'These widgets will be shown in the Secondary Sidebar.',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );

		register_sidebar( array(
			'name' => "Top Sidebar",
			'id' => 'top-sidebar',
			'description' => 'These widgets will be shown in the Secondary Sidebar.',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
	) );

    register_sidebar( array(
        'name' => "Footer Sidebar 1",
        'id' => 'footer-sidebar-1',
        'description' => 'These widgets will be shown in the Footer.',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );

    register_sidebar( array(
        'name' => "Footer Sidebar 2",
        'id' => 'footer-sidebar-2',
        'description' => 'These widgets will be shown in the Footer.',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );

    register_sidebar( array(
        'name' => "Footer Sidebar 3",
        'id' => 'footer-sidebar-3',
        'description' => 'These widgets will be shown in the Footer.',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ) );

    register_sidebar( array(
				'name' => "Footer Sidebar 3",
				'id' => 'footer-sidebar-3',
				'description' => 'These widgets will be shown in the Footer.',
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget' => '</section>',
				'before_title' => '<h2>',
				'after_title' => '</h2>'
		) );
}


//Shortcode [latest_posts]
function livespiltips_latest_posts_shortcode_function( $atts ){
    extract( shortcode_atts( array(
        'post_to_show' => '4',
        'category' => 'Uncategorized',
    ), $atts ) );

    ob_start();

//     $post_to_show = $post_to_show + 1;

    $params = array(
			'category_name'  => $category,
			'post_type'      => 'post',
			'orderby'        => 'date',
			'order'          => 'DESC'
		);

    $query = new WP_Query( array_merge($params, array(
			'offset'         => 1,
			'posts_per_page' => $post_to_show,
			'meta_query'     => array(
				array(
					'key'     => 'start_date',
					'value'   => date("Y-m-d H:i:s"),
					'compare' => '>=',
					'type'    => 'DATETIME'
					)
				),
    )));

//     if ($query->post_count < $post_to_show) {
// 			$query2 = new WP_Query( array_merge($params, array(
// 				'offset'         => +($post_to_show == $query->post_count),
// 				'posts_per_page' => $post_to_show - $query->post_count,
// 				'meta_query'     => array( array( 'key' => 'start_date', 'compare' => 'NOT EXISTS' ) )
// 			)));

// 			$query->posts      = array_merge( $query2->posts, $query->posts );
// 			$query->post_count = count( $query->posts );
//     }

    if ( $query->have_posts() ) { ?>
          <div class="livespiltips-latest-post-list">
                <?php
                    $index = 0;
                    while ( $query->have_posts() ) : $query->the_post();
                    $index++;
                    $className = '';
                    $thumbnail_default = "https://livespiltips.dk/wp-content/uploads/2020/11/default_post.jpg";
                    $large_thumbnail_src = get_the_post_thumbnail_url( get_the_ID(), 'latest-large-post-thumb' );
                    $thumbnail_src = get_the_post_thumbnail_url( get_the_ID(), 'latest-post-thumb' );
                    $thumbnail = $thumbnail_src;
                    $post_link = get_page_link();
										$isPostThumbnail = get_the_post_thumbnail();
										$post_image = $isPostThumbnail ? $large_thumbnail_src : $thumbnail_default;
										$post_image_large = $isPostThumbnail ? $thumbnail : $thumbnail_default;
										$meta = get_post_meta(get_the_ID());
                    $home_team_logo_small_url  = isset($meta['home_logo_small']) ? $meta['home_logo_small'][0] : '';
                    $away_team_logo_small_url  = isset($meta['away_logo_small']) ? $meta['away_logo_small'][0] : '';
                    $home_team_logo_medium_url = isset($meta['home_logo']) ? $meta['home_logo'][0] : '';
                    $away_team_logo_medium_url = isset($meta['away_logo']) ? $meta['away_logo'][0] : '';
                    $home_team_logo_url        = empty($home_team_logo_small_url) ? $home_team_logo_medium_url : $home_team_logo_small_url;
                    $away_team_logo_url        = empty($away_team_logo_small_url) ? $away_team_logo_medium_url : $away_team_logo_small_url;
                    $start_date = isset($meta['start_date']) ? $meta['start_date'][0] : get_the_date();
                    $postDate = new DateTime($start_date);
                    $DD = $postDate->format('j. M');
                ?>
                 <div class=<?php echo $className?>>
                     <figure>
                     		<div>
													<img width="249" height="180" <?php $isPostThumbnail ? printf( 'srcset="%s 1x, %s 2x, %s 3x"', $post_image_large, $post_image_large, $post_image_large ) : ''; ?> src=<?php echo $post_image; ?> alt="Event" />
                        </div>
											<?php if ( !empty($home_team_logo_url) && !empty($away_team_logo_url) )  { ?>
												<div class="post_teams_logo">
													<img width="50" height="50" src=<?php echo $home_team_logo_url; ?> alt="Home team logo" />
													<img width="50" height="50" src=<?php echo $away_team_logo_url; ?> alt="Away team logo" />
												</div>
												<?php } ?>
		                <time><?php echo $DD ?></time>
                        <figcaption>
                            <h2><a href=<?php echo $post_link ?>> <?php the_title(); ?> </a></h2>
                        </figcaption>
                     </figure>
                 </div>
                <?php endwhile;
                wp_reset_postdata(); ?>
        </div>

        <?php $content = ob_get_clean();
        return $content;
    }
}

//Shortcode [fullwidth_latest_post]
function livespiltips_fullwidth_latest_post_shortcode_function( $atts ){
    extract( shortcode_atts( array(
        'button_text' => 'se spiltip',
        'category' => 'Uncategorized',
    ), $atts ) );

    ob_start();

    $params = array(
			'posts_per_page' => 1,
			'category_name'  => $category,
			'post_type'      => 'post',
			'orderby'        => 'date',
			'order'          => 'DESC',
		);

    $query = new WP_Query( array_merge($params, array(
			'meta_query' => array(
				array(
					'key'     => 'start_date',
					'value'   => date("Y-m-d H:i:s"),
					'compare' => '>=',
					'type'    => 'DATETIME'
					)
				)
    ) ) );

    if ($query->post_count < 1) {
			$query = new WP_Query( array_merge($params, array( 'meta_query' => array( array( 'key' => 'start_date', 'compare' => 'NOT EXISTS' ) ) ) ) );
    }

    if ( $query->have_posts() ) { ?>
          <div class="livespiltips-fullwidth-latest-post">
                <?php
                    $index = 0;
                    while ( $query->have_posts() ) : $query->the_post();
                    $index++;
                    $thumbnail_default = "https://livespiltips.dk/wp-content/uploads/2020/11/default_post.jpg";
                    $thumbnail = get_the_post_thumbnail_url( get_the_ID(), 'fullwidth-latest-post-thumb' );
                    $thumbnail_mobile = get_the_post_thumbnail_url( get_the_ID(), 'latest-post-thumb' );
                    $post_link = get_page_link();
                    $isPostThumbnail = get_the_post_thumbnail();
                    $post_image = $isPostThumbnail ? $thumbnail : $thumbnail_default;
                    $post_image_mobile = $isPostThumbnail ? $thumbnail_mobile : $thumbnail_default;
                    $meta = get_post_meta(get_the_ID());
                    $home_team_logo_small_url  = isset($meta['home_logo_small']) ? $meta['home_logo_small'][0] : '';
                    $away_team_logo_small_url  = isset($meta['away_logo_small']) ? $meta['away_logo_small'][0] : '';
                    $home_team_logo_medium_url = isset($meta['home_logo']) ? $meta['home_logo'][0] : '';
                    $away_team_logo_medium_url = isset($meta['away_logo']) ? $meta['away_logo'][0] : '';
                    $home_team_logo_url        = empty($home_team_logo_small_url) ? $home_team_logo_medium_url : $home_team_logo_small_url;
                    $away_team_logo_url        = empty($away_team_logo_small_url) ? $away_team_logo_medium_url : $away_team_logo_small_url;
                    $start_date = isset($meta['start_date']) ? $meta['start_date'][0] : get_the_date();
                    $postDate = new DateTime($start_date);
                    $DD = $postDate->format('j M Y');
                ?>
                 <div>
									 <figure>
											<picture>
												<source srcset=<?php echo $post_image; ?> media="(min-width: 501px)">
												<img width="249" height="180" src=<?php echo $post_image_mobile; ?> alt="First Event" />
											</picture>
											<?php if ( !empty($home_team_logo_url) && !empty($away_team_logo_url) )  { ?>
												<div class="post_teams_logo">
													<img width="50" height="50" src=<?php echo $home_team_logo_url; ?> alt="Home team logo" />
													<img width="50" height="50" src=<?php echo $away_team_logo_url; ?> alt="Away team logo" />
												</div>
												<?php } ?>
											<figcaption>
												<h2><a href=<?php echo $post_link ?>> <?php the_title(); ?> </a></h2>
												<div>
													<time><?php echo $DD ?></time>
													<a class="read-more" href=<?php echo $post_link ?>><?php echo $button_text ?> <i></i></a>
												</div>
											</figcaption>
									 </figure>
                 </div>
                <?php endwhile;
                wp_reset_postdata(); ?>
        </div>

        <?php $content = ob_get_clean();
        return $content;
    }
}

//Shortcode [featured_posts]
function livespiltips_featured_posts_shortcode_function( $atts ){
    extract( shortcode_atts( array(
        'post_to_show' => '6',
        'category' => 'Uncategorized',
    ), $atts ) );

    ob_start();
    $query = new WP_Query( array(
        'posts_per_page'   => $post_to_show,
        'category_name'    => $category,
        'post_type'        => 'post',
        'orderby'          => 'date',
        'order'            => 'DESC',
    ) );

    if ( $query->have_posts() ) { ?>
          <ul class="livespiltips-featured-post-list">
                <?php
                    while ( $query->have_posts() ) : $query->the_post();
                    $imgurl = get_the_post_thumbnail_url( get_the_ID(), 'latest-post-thumb' );
                    $date = get_the_date();
                    $postDate = new DateTime($date);
                    $DD = $postDate->format('j. M');
                    $post_link = get_page_link();
                ?>
                 <li>
                     <figure>
                        <div><img src=<?php echo $imgurl;?>></div>
                        <figcaption>
                            <time><?php echo $DD ?></time>
                            <h2><a href=<?php echo $post_link ?>> <?php the_title(); ?> </a></h2>
                        </figcaption>
                     </figure>
                 </li>
                <?php endwhile;
                wp_reset_postdata(); ?>
        </ul>

        <?php $content = ob_get_clean();
        return $content;
    }
}

function livespiltips_dns_prefetch_to_preconnect( $urls, $relation_type ) {
	if ( 'dns-prefetch' === $relation_type ) {
		$urls = [];
	}
	if ( 'preconnect' === $relation_type ) {
		$urls = wp_dependencies_unique_hosts();
		array_push($urls, 'https://www.google-analytics.com');
	}
	return $urls;
}
add_filter( 'wp_resource_hints', 'livespiltips_dns_prefetch_to_preconnect', 0, 2 );

function livespiltips_disable_emoji_feature() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji');
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji');
	remove_filter( 'embed_head', 'print_emoji_detection_script' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'livespiltips_disable_emojis_tinymce' );
	add_filter( 'option_use_smilies', '__return_false' );
}
function livespiltips_disable_emojis_tinymce( $plugins ) {
	if( is_array($plugins) ) {
		$plugins = array_diff( $plugins, array( 'wpemoji' ) );
	}
	return $plugins;
}
add_action('init', 'livespiltips_disable_emoji_feature');

function livespiltips_smartwp_remove_wp_block_library_css() {
  if( is_front_page() || is_home() ){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' );
	}
}
add_action( 'wp_enqueue_scripts', 'livespiltips_smartwp_remove_wp_block_library_css', 100 );
add_action( 'enqueue_block_assets', 'livespiltips_smartwp_remove_wp_block_library_css', 100 );

function livespiltips_remove_gutenberg_styles($translation, $text, $context, $domain) {
	if ( is_front_page() || is_home() ) {
		if($context != 'Google Font Name and Variants' || $text != 'Noto Serif:400,400i,700,700i') {
			return $translation;
		}
	}
	return $translation;
}
add_filter( 'gettext_with_context', 'livespiltips_remove_gutenberg_styles',10, 4);

function livespiltips_disable_gutenberg() {
  if( is_front_page() || is_home() ) {
    remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );
    remove_action( 'admin_enqueue_scripts', 'wp_common_block_scripts_and_styles' );
  }
}
add_action('wp', 'livespiltips_disable_gutenberg');

function livespiltips_add_preload_fonts() {
	// echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Montserrat-Regular.woff2' . '" as="font" crossorigin>';
	// echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Montserrat-Medium.woff2' . '" as="font" crossorigin>';
	echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Montserrat-Bold.woff2' . '" as="font" crossorigin>';
}
add_action( 'wp_head', 'livespiltips_add_preload_fonts', 5);

function livespiltips_mihdan_add_defer_attribute( $tag, $handle ) {
  $handles = array( 'livespiltips-navigation' );

   foreach( $handles as $defer_script) {
      if ( $defer_script === $handle ) {
         return str_replace( ' src', ' defer  src', $tag );
      }
   }

   return $tag;
}
add_filter( 'script_loader_tag', 'livespiltips_mihdan_add_defer_attribute', 10, 2 );
