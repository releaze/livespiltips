<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package livespiltips
 */
?>
	</div><!-- #content -->
	<footer id="colophon" class="site-footer">
        <div class="container">
            <div class="site-info">
                <div class="row">
                    <div class="col-md-6">
                        <?php dynamic_sidebar( 'footer-sidebar-1' ); ?>
                    </div>
                    <div class="col-md-6">
                        <?php dynamic_sidebar( 'footer-sidebar-2' ); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="fullwidth-area">
            <div class="row">
                <div class="col-md-12">
                    <?php dynamic_sidebar( 'footer-sidebar-3' ); ?>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright-area">
                <div class="row">
                    <div class="col-md-12">
                         Copyright &copy; <?php echo date('Y');?> - <?php echo get_option( 'blogname' );?>
                    </div>
                </div>
            </div>
        </div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
