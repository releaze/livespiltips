<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package livespiltips
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="page" class="site">
    <header class='livespiltips-site-header'>
        <div class='site-branding-container'>
            <div class='container'>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            </div>
        </div>

				 <div class="skip-link screen-reader-text">
						<a href="#content" title="<?php esc_attr_e( 'Skip to content', 'tutsplus' ); ?>">
								<?php _e( 'Skip to content', 'tutsplus' ); ?>
						</a>
				</div>

				<div id="menu-toggle" >
					<label for="open_mobile_menu" class="screen-reader-text">Open mobile menu</label>
					<input type="checkbox" id="open_mobile_menu" name="open_mobile_menu" label="Open mobile menu" />
					<span></span>
					<span></span>
					<span></span>

					<nav id="main-menu" class='container main-menu'>
							<?php
									wp_nav_menu( [
											'theme_location'  => 'livespiltips_top_menu',
											'container'       => null,
											'menu_class'      => 'nav nav-menu',
											'menu_id'         => 'nav',
									] );
							?>
					</nav>
				</div>
    </header>
		<div class="widget-area top-sidebar">
				<?php dynamic_sidebar( 'top-sidebar' ); ?>
		</div>
