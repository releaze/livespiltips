<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package livespiltips
 */

?>

<?php
		$content_post = get_post(get_the_ID());
		$content = $content_post->post_content;
		$paragraphStart = strpos($content, '<p');
		$widgetStart = strpos($content, '<si-lb-widget');
		$hasLeadingWidget = $widgetStart && $widgetStart < $paragraphStart;	
	?>

<article id="post-<?php the_ID(); ?>" <?php post_class($hasLeadingWidget  ? "post-leading-widget" : ""); ?>>
	<div class="entry-group">
		<header class="entry-header">
			
			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			?>
			<?php
				if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
					livespiltips_posted_on();
					livespiltips_posted_by();
					?>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<?php livespiltips_post_thumbnail(); ?>
	</div>
	
	<div class="entry-content <?= $hasLeadingWidget  ? "entry-content-leading-widget" : "" ?>">

		<?php		
			
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'livespiltips' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );
 


		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'livespiltips' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
			
	</footer><!-- .entry-footer -->


	<?php the_post_navigation(); ?>
</article><!-- #post-<?php the_ID(); ?> -->
