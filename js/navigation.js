(function() {
  const siteNavigation = document.getElementById("main-menu");
  if (!siteNavigation) {
    return;
  }
  const linksWithChildren = siteNavigation.querySelectorAll(
    ".menu-item-has-children > a, .page_item_has_children > a"
  );
  for (const link of linksWithChildren) {
    link.addEventListener("touchstart", toggleFocus, { passive: true });
  }
  function toggleFocus() {
    const menuItem = this.parentNode;
    for (const link of menuItem.parentNode.children) {
      if (menuItem !== link) {
        link.classList.remove("focus");
      }
    }
    menuItem.classList.toggle("focus");
  }
})();
